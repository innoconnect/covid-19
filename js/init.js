function vis(data) {
    WGL.init(data.age.length, "lib/webglayer/", "map", true);
    let svg = new MapBoxD3("data/okresy.geojson", data);
    //aaa.draw()

    $("#min-about").off("click").on("click", function () {
        $("#min-about").toggleClass("btn-plus");
        $($("#about")[0]).slideToggle();

        if ($("#about-chevron").text() === "keyboard_arrow_down") {
            $("#about-chevron").text("keyboard_arrow_up");
            $("#right").scrollTop(0);
        } else {
            $("#about-chevron").text("keyboard_arrow_down");
        }
    });
    $("#min-stats").off("click").on("click", function () {
        $("#min-stats").toggleClass("btn-plus");
        $($("#stats")[0]).slideToggle();

        if ($("#stats-chevron").text() === "keyboard_arrow_down") {
            $("#stats-chevron").text("keyboard_arrow_up");
            $("#right").scrollTop(0);
        } else {
            $("#stats-chevron").text("keyboard_arrow_down");
        }
    });
    $(".chart-filters-area i").off("click").on("click", function () {
        draw.deleteAll();
        draw.changeMode(draw.modes.SIMPLE_SELECT);

        if (WGL.getDimension('themap').getVisible() && draw.getAll().features.length === 0) {
            var idt = WGL.getDimension('idt');
            idt.setEnabled(true);
        }

        $("#controlsToggle .btn-selected").removeClass("btn-selected");
        WGL.filterDim('themap', 'polybrush', []);
        $("#chd-container-footer-area").addClass("hide");

        if ($(".active-filters-container [id^=chd-container-footer]:not(.hide)").length > 0) {
            $(".close-item-filters").removeClass("hide");
            $("#active-filters-placeholder").addClass("hide");
            $(".active-filters-item .bar-item").addClass("bar-item-active");
            $(".active-filters-container").slideDown();
        } else {
            $("#active-filters-placeholder").removeClass("hide");
            $(".close-item-filters").removeClass("hide");
        }
    });
    $(".close-item-btn").off("click").on("click", function () {
        $(this).hide();
        $(this).parent().slideToggle();
        draw.changeMode(draw.modes.SIMPLE_SELECT);
        $(this).parent().parent().find(".bar-item").toggleClass("bar-item-active");
    });
    $(".active-filters-item").off("click").on("click", function () {
        $(".active-filters-container").slideToggle();
        $(".active-filters-container .close-item-btn").show();
        $(this).children(".bar-item").toggleClass("bar-item-active");
    });
    $(".legend-item").off("click").on("click", function () {
        $(".legend-container").slideToggle();
        $(".legend-container .close-item-btn").show();
        $(this).children(".bar-item").toggleClass("bar-item-active");
    });
    $(".active-filters-item").mouseover(function () {
        $(this).children(".bar-item").addClass("bar-item-hover");
    });
    $(".area-item").mouseover(function () {
        $(this).children(".bar-item").addClass("bar-item-hover");
    });
    $(".layers-item").mouseover(function () {
        $(this).children(".bar-item").addClass("bar-item-hover");
    });
    $(".legend-item").mouseover(function () {
        $(this).children(".bar-item").addClass("bar-item-hover");
    });
    $(".heatmap-item").mouseover(function () {
        $(this).children(".bar-item").addClass("bar-item-hover");
    });
    $(".charts-item").mouseover(function () {
        $(this).children(".bar-item").addClass("bar-item-hover");
    });
    $(".active-filters-item").mouseout(function () {
        $(this).children(".bar-item").removeClass("bar-item-hover");
    });
    $(".area-item").mouseout(function () {
        $(this).children(".bar-item").removeClass("bar-item-hover");
    });
    $(".layers-item").mouseout(function () {
        $(this).children(".bar-item").removeClass("bar-item-hover");
    });
    $(".heatmap-item").mouseout(function () {
        $(this).children(".bar-item").removeClass("bar-item-hover");
    });
    $(".legend-item").mouseout(function () {
        $(this).children(".bar-item").removeClass("bar-item-hover");
    });
    $(".charts-item").mouseout(function () {
        $(this).children(".bar-item").removeClass("bar-item-hover");
    });

    var statsDiv = $("#chd-container-stats").detach();
    var charts = {};

    var params = {
        classes_mask: ["selected", "unselected"],
        classes_legend: ["", "<b>Unselected</b> data and data <b>out</b> of the current map view", "<b>Selected</b> data within the current map view", "Click on the 'zoom to' icon to adjust the chart scale to the 'selected' or 'unselected' data"],
        permalink_input: "permalink-input",
        rotate_x: true,
        w: 600,
        h: 300,
        margin: {
            top: 37,
            right: 70,
            bottom: 65,
            left: 60
        },
        hideLegend: true
    };

    /*LAST 30 DAYS*/
    // const day_last = {
    //     data: data.day,
    //     domain: data.dayEnum,
    //     name: 'dayl',
    //     type: 'ordinal',
    //     label: "Den"
    // };
    //
    // var chd7 = new WGL.ChartDiv("charts", "ch7", "Den", "", data.dayEnum.length);
    // var dim7 = WGL.addOrdinalHistDimension(day_last);
    // chd7.setDim(dim7);
    // WGL.addLinearFilter(day_last, data.dayEnum.length, 'daylF');
    //
    // let restricted_doamin = data.dayEnum.slice(data.dayEnum.length - 31, data.dayEnum.length);
    // charts['dayl'] = new WGL.ui.StackedBarChart(day_last, "ch7", "den", 'daylF', params, restricted_doamin);
    //
    // charts['dayl'].xformat = function (d) {
    //     let index = data.dayEnum.indexOf(d);
    //     if(index%1 === 0){
    //         return d;
    //     }
    //     return "";
    // };

    /*DAY CUMSUM*/
    //const day2 = {
    //    data: data.day,
    //    domain: data.dayEnum,
    //    name: 'day2',
    //    type: 'ordinal',
    //    label: "Den"
    //};

    //var paramsDay = {
    //    classes_mask: ["selected", "unselected"],
    //    classes_legend: ["", "<b>Unselected</b> data and data <b>out</b> of the current map view", "<b>Selected</b> data within the current map view", "Click on the 'zoom to' icon to adjust the chart scale to the 'selected' or 'unselected' data"],
    //    permalink_input: "permalink-input",
    //    rotate_x: true,
    //    w: 600,
    //    cumsum: true,
    //    cumsum_dependence: "day",
    //    charts: charts,
    //    hideLegend: true
    //};

    //var chd6 = new WGL.ChartDiv("charts", "ch6", "Den", "", data.dayEnum.length);
    //var dim6 = WGL.addOrdinalHistDimension(day2);
    //chd6.setDim(dim6);
    //WGL.addLinearFilter(day2, data.dayEnum.length, 'day2F');
    //charts['day2'] = new WGL.ui.StackedBarChart(day2, "ch6", "den", 'day2F', paramsDay);

    //charts['day2'].xformat = function (d) {
    //    let index = data.dayEnum.indexOf(d);
    //    if(index%14 === 0){
    //        return d;
    //    }
    //    return "";
    //};

    /*DAY*/
    const day = {
        data: data.day,
        domain: data.dayEnum,
        name: 'day',
        type: 'ordinal',
        label: "Den"
    };

    var chd5 = new WGL.ChartDiv("charts", "ch5", "Den", "", data.dayEnum.length);
    var dim5 = WGL.addOrdinalHistDimension(day);
    chd5.setDim(dim5);
    WGL.addLinearFilter(day, data.dayEnum.length, 'dayF');
    charts['day'] = new WGL.ui.StackedBarChart(day, "ch5", "den", 'dayF', params);

    charts['day'].xformat = function (d) {
        let index = data.dayEnum.indexOf(d);
        if(index%7 === 0){
            return d;
        }
        return "";
    };

    /*AGE*/
    const age = {
        data: data.age,
        min: 0,
        max: 99,
        num_bins: 10,
        name: 'age',
        type: 'linear',
        label: "Věk"
    };

    var paramsAge = {
        classes_mask: ["selected", "unselected"],
        classes_legend: ["", "<b>Unselected</b> data and data <b>out</b> of the current map view", "<b>Selected</b> data within the current map view", "Click on the 'zoom to' icon to adjust the chart scale to the 'selected' or 'unselected' data"],
        permalink_input: "permalink-input",
        rotate_x: false,
        w: 600,
        h: 300,
        margin: {
            top: 37,
            right: 70,
            bottom: 65,
            left: 60
        },
        hideLegend: true
    };

    var chd3 = new WGL.ChartDiv("charts", "ch3", "Věk", "", 10);
    var dim3 = WGL.addLinearHistDimension(age);
    chd3.setDim(dim3);
    WGL.addLinearFilter(age, 10, 'ageF');
    charts['age'] = new WGL.ui.StackedBarChart(age, "ch3", "Věk", 'ageF', paramsAge);

    /*COUNTRY*/
    var country = {
        data: data.country,
        domain: data.countryEnum,
        name: 'country',
        type: 'ordinal',
        label: "Nákaza se zahraničí",
        dateRangeStart: data.dateRangeStart,
        dateRangeEnd: data.dateRangeEnd
    };

    var paramsCountries = {
        classes_mask: ["selected", "unselected"],
        classes_legend: ["", "<b>Unselected</b> data and data <b>out</b> of the current map view", "<b>Selected</b> data within the current map view", "Click on the 'zoom to' icon to adjust the chart scale to the 'selected' or 'unselected' data"],
        permalink_input: "permalink-input",
        rotate_x: true,
        w: 600,
        h: 300,
        margin: {
            top: 37,
            right: 90,
            bottom: 65,
            left: 60
        },
        hideLegend: true
    };

    var chd4 = new WGL.ChartDiv("charts", "ch4", "Nákaza se státu", "", data.countryEnum.length);
    var dim4 = WGL.addOrdinalHistDimension(country);
    chd4.setDim(dim4);
    WGL.addLinearFilter(country, data.countryEnum.length, 'countryF');
    const rest = ["DE", "UA", "SK", "AT", "EG", "TZ", "RU", "AE", "PL", "ES", "IT", "CH", "RS", "BG", "GB", "FR", "other"];
    charts['country'] = new WGL.ui.StackedBarChart(country, "ch4", "Státy", 'countryF', paramsCountries, rest);

    d3.select("#ch4").append("div").attr("id", "country-desc")
        .style("left", "40px")
        .style("top", "10px")
        .style("padding", "15px").style("position", "relative");

    /*REGIONS*/
    var paramsRegions = {
        classes_mask: ["selected", "unselected"],
        classes_legend: ["", "<b>Unselected</b> data and data <b>out</b> of the current map view", "<b>Selected</b> data within the current map view", "Click on the 'zoom to' icon to adjust the chart scale to the 'selected' or 'unselected' data"],
        permalink_input: "permalink-input",
        rotate_x: true,
        showSelectedOnly: true,
        w: 600,
        h: 250,
        margin: {
            top : 20,
            right : 70,
            bottom : 90,
            left : 60
        },
        hideLegend: true
    };
    var region = {
        data: data.nuts3,
        domain: data.nuts3Enum,
        name: 'kraje',
        type: 'ordinal',
        label: "Kraj",
        dateRangeStart: data.dateRangeStart,
        dateRangeEnd: data.dateRangeEnd
    };

    // var chd1 = new WGL.ChartDiv("charts", "ch1", "Kraj", "", data.nuts3Enum.length);
    var dim1 = WGL.addOrdinalHistDimension(region);
    //chd1.setDim(dim1);
    WGL.addLinearFilter(region, data.nuts3Enum.length, 'krajeF');
    // charts['kraje'] = new WGL.ui.StackedBarChart(region, "ch1", "day of the week", 'krajeF', paramsRegions);

    // const nuts2name = {
    //     "CZ010": "Praha",
    //     "CZ020": "Středočeský",
    //     "CZ041": "Karlovarský",
    //     "CZ042": "Ústecký",
    //     "CZ032": "Plzeňský",
    //     "CZ031": "Jihočeský",
    //     "CZ051": "Liberecký",
    //     "CZ052": "Královéhradecký",
    //     "CZ053": "Pardubický",
    //     "CZ063": "Vysočina",
    //     "CZ064": "Jihomoravský",
    //     "CZ072": "Zlínský",
    //     "CZ071": "Olomoucký",
    //     "CZ080": "Moravskoslezský"
    // };
    // charts['kraje'].xformat = function (d) {
    //   return d;
    // };

    /*SEX*/
    const sex = {
        data: data.sex,
        domain: data.sexEnum,
        name: 'sex',
        type: 'ordinal',
        label: "Pohlaví",
        dateRangeStart: data.dateRangeStart,
        dateRangeEnd: data.dateRangeEnd
    };

    var params2 = {
        classes_mask: ["selected", "unselected"],
        classes_legend: ["", "<b>Unselected</b> data and data <b>out</b> of the current map view", "<b>Selected</b> data within the current map view", "Click on the 'zoom to' icon to adjust the chart scale to the 'selected' or 'unselected' data"],
        permalink_input: "permalink-input",
        rotate_x: false,
        w: 600,
        h: 320,
        margin: {
            top: 100,
            right: 70,
            bottom: 65,
            left: 60
        },
        hideLegend: true
    };

    var chd2 = new WGL.ChartDiv("charts", "ch2", "Pohlaví", "", 14);
    var dim2 = WGL.addOrdinalHistDimension(sex);
    chd2.setDim(dim2);
    WGL.addLinearFilter(sex, 2, 'sexF');
    charts['sex'] = new WGL.ui.StackedBarChart(sex, "ch2", "pohlaví", 'sexF', params2);
    new WGL.FilterListChartDiv("filters-container", charts['sex'], "pohlaví", 2); //chd1.change();

    svg.afterLoad = function () {
        WGL.addCharts(charts);
        WGL.initFilters();
        WGL.render();
        $("#charts-menu").click();
    };

    svg.draw();

    $("#language-section").off("click").on("click", function () {
        return $("#language-options").slideToggle();
    });
    $("#background-section").off("click").on("click", function () {
        return $("#background-options").slideToggle();
    });
    $("#advanced-section").off("click").on("click", function () {
        return $("#advanced-options").slideToggle();
    });
    $("#about-section").off("click").on("click", function () {
        $("#legend-win").hide();
        $("#about-win").css("display") === "block" ? $(".overlay").hide() : $(".overlay").show();
        $("#about-win").show();
    });
    $("#legend-section").off("click").on("click", function () {
        $("#about-win").hide();
        $("#legend-win").css("display") === "block" ? $(".overlay").hide() : $(".overlay").show();
        $("#legend-win").show();
    });
    $(".language-option").off("click").on("click", e => {selectLanguage($(e.target).attr("data-language"))});
    $("#charts-menu, #charts-title").off("click").on("click", function () {
        $("#right").animate(
            {
                width: 'toggle'
            }
         );
        updateChartsVisibility();
    });
    $(".wgl-intro-close, .click-to-continue", "#intro-win").off("click").one("click", function (e) {
        return clickToContinue();
    });
    setTimeout(function () {
        $(".click-to-continue").addClass("click-to-continue-active");
    }, 1500);
    $("#loading").hide();

    if ($("#intro-win").css("display") === "none") {
        $(".overlay").hide();
    }

    $(".hamburger-menu, #navbar-title").off("click").on("click", function () {
        $("#side-navbar-content").animate({
            width: 'toggle'
        });
    });

    $(".overlay").hide().click(e => {
        if($(e.target).hasClass("overlay") || $(e.target).hasClass("win-wrapper")) {
            $("#about-win").hide();
            $(".overlay").hide();
        }
    });

    $(".wgl-warning-close").click(() => {
        $("#warning-win").hide();
        $(".overlay").hide();
    });

    $(".wgl-about-close").click(() => {
        $("#about-win").hide();
        $(".overlay").hide();
    });

    $("#export-section").off("click").on("click", () => {
        $("#c").hide();
        $(".export-section-icon").toggle("hide");
        $(".export-section-spinner").toggle("hide");
        setTimeout(() => {
            toCanvas(() => {
                $(".export-section-icon").toggle("hide");
                $(".export-section-spinner").toggle("hide");
            });
            $("#c").show();
        }, 500);
    });

    //$("#ch1").append($('<div style="position: relative; text-align: center; padding: 10px 0;"><span id="days-chart-message">Average amount of vehicles on selected street(s) per day</span><span id="days-chart-tooltip" class="top-help-icon" title="The chart displays the average daily traffic on the\n streets that are currently visible on the map (or only\n on streets selected from the map). The average is\n calculated only for the time period(s) selected in the\n other filters (dates, hours).\n\n Orange colour indicates the selected hours. Blue those\n that are not selected.\n\n Note: If you select a single date and one street\n segment, the figures displayed in the chart are the\n real counts of vehicles measured by the detectors on\n that day (i.e. not averages)."><i class="material-icons tooltipstered">help</i></span></div>'));
    //$("#ch4").append($('<div style="position: relative; text-align: center; padding: 10px 0;"><span id="hours-chart-message">Average amount of vehicles on selected street(s) per hour</span><span id="hours-chart-tooltip" class="top-help-icon" title="The chart displays the average traffic per hour on the\n streets that are currently visible on the map (or only\n on streets selected from the map). The average is\n calculated only for the time period(s) selected in the\n other filters (dates, days).\n\n Orange colour indicates the selected hours. Blue those\n that are not selected.\n\n Note: If you select a single date and one street\n segment, the figures displayed in the chart are the\n real counts of vehicles measured by the detectors on\n that day (i.e. not averages)."><i class="material-icons tooltipstered">help</i></span></div>'));
    statsDiv.appendTo("#charts");
    var charts_element = $("#charts");
    charts_element.sortable({
        placeholder: "ui-state-highlight",
        handle: '.chart-header',
        zIndex: 9999,
        helper: 'clone',
        cursor: "move",
        start: function start() {
            $(this).find(".chart-header").addClass('grabbing');
        },
        stop: function stop() {
            $(this).find(".chart-header").removeClass('grabbing');
        },
        update: function update(event, ui) {
            var idPreviousSibling = $($(ui.item[0]).prev()).attr("id");
            $("#chd-container-" + $($(ui.item[0]).children(".chart-content")[0]).attr("id") + " .chart-content").css("visibility", "visible");
            var element = $("#chd-container-footer-" + $($(ui.item[0]).children(".chart-content")[0]).attr("id")).detach();

            if (typeof idPreviousSibling !== "undefined") {
                $("#chd-container-footer-" + $("#" + idPreviousSibling).children(".chart-content").attr("id")).after(element);
            } else {
                $("#filters-container").prepend(element);
            }

            updateChartsVisibility();
        }
    });

    $("#bottomLegendWrapper").fadeIn();

    setTimeout(() => {
        selectLanguage("cz");
    }, 2000);

    WGL.registerUpdateFunction(() => {
        automaticZoomCharts();
        setResetFiltersBtnColor();
    });
    // select last 14 days
    WGL.filterDim("dayl", "daylF", [[data.dayEnum.length - 14, data.dayEnum.length]]);

    setTimeout(() => {
        d3.select("#minch7").select(".chart-filters-selected").text("14");
    }, 2000);

}

function setResetFiltersBtnColor() {
    if(WGL.getNumberOfActiveFilters() === 0) {
        $("#restart-button").css("background-color","#8CC5F9");
    } else {
        $("#restart-button").css("background-color","#ffa91b");
    }
}

function automaticZoomCharts() {
    if (WGL.getNumberOfActiveFilters() !== 0) {
        $(".select-legend-scale").attr("data-previously-selected", "true");

        if (!$(".legend-selected").hasClass("select-legend-scale")) {
            $(".legend-selected i").click();
        }
    } else {
        $(".legend-selected i").click();
        var chart_contents = $(".chart-content");

        for (var i = 0; i < chart_contents.length; i++) {
            if ($("#" + $($(".chart-content")[i]).attr("id") + " [data-previously-selected=true]").length > 0 && !$("#" + $($(".chart-content")[i]).attr("id") + " [data-previously-selected=true]").hasClass("legend-selected")) {
                $("#" + $($(".chart-content")[i]).attr("id") + " [data-previously-selected=true] i").click();
            } else {
                $("#" + $($(".chart-content")[i]).attr("id") + " .legend-unselected i").click().click();
            }
        }

        $(".select-legend-scale").removeAttr("data-previously-selected");
    }
}

function selectLanguage(language) {
    window.languageActive = language;
    $(".language-option").removeClass("layer-selected");
    $("#language-option-"+language).addClass("layer-selected");
    const data = new DataLoader();
    data.loadLanguageData('data/languages/'+language+'.json');
}

function renderLanguage(languageData) {

    $("#country-desc").html(languageData['country-desc']);

    $(".language-select").val(languageData['language']);

    $(".wgl-about-context").html(languageData['about']);

    $(".wgl-warning-context").html(languageData['warning']);

    $(".wgl-about-head span").text(languageData['about-title']);

    //document.title = capitalizeString(languageData['app-title']);
    document.title = languageData['app-title'];

    $(".charts-item-title").html(languageData['navitem-charts']);

    $("#active-filters-placeholder").html(languageData['empty-filters-msg']);

    $("#about-text").html(languageData['about-section']);
    $("#feedback-section-text").text(languageData['feedback-section']);
    $("#language-section-text").html(languageData['language-section']);
    $("#export-section-text").html(languageData['export-section']);
    $("#side-navbar-title").html(languageData['side-navbar-title']);
    $("#hamburger-menu").prop('title', languageData['expand-settings-panel']);
    $("#charts-menu").prop('title', languageData['expand-charts-panel']);
    $("#charts-panel-title").html(languageData['charts-panel-title']);

    $("#charts-about").html(languageData['app-sidebar-description']);

    $("#restart-button").text(languageData['reset-all-filters-button']);

    $("#bottomLegendText").text(languageData['heatmap-legend-footer']);

    $(".region-relative-cases-text").text(languageData['region-relative-cases-text']);
    $(".region-cases-text").text(languageData['region-cases-text']);

    $("#chd-container-ch_switch .chart-title text").text(languageData['chart-switch-title']);
    $("#chd-container-ch1 .chart-title text").text(languageData['chart-1-title']);
    $("#chd-container-ch2 .chart-title text").text(languageData['chart-2-title']);
    $("#chd-container-ch3 .chart-title text").text(languageData['chart-3-title']);
    $("#chd-container-ch4 .chart-title text").text(languageData['chart-4-title']);
    $("#chd-container-ch5 .chart-title text").text(languageData['chart-5-title']);
    $("#chd-container-ch6 .chart-title text").text(languageData['chart-6-title']);
    $("#chd-container-ch7 .chart-title text").text(languageData['chart-7-title']);


    $(".chart-filters-clean").prop('title', languageData['tooltip-clear-selection']);
    $("div.legend-selected").prop('title', languageData['tooltip-zoom-selected-data']);
    $("div.legend-unselected").prop('title', languageData['tooltip-zoom-unselected-data']);
    $("div.legend-out").prop('title', languageData['tooltip-zoom-out-data']);
    $(".chart-title-footer text").prop('title', languageData['tooltip-go-to-chart']);

    $(".chart-header").prop('title', languageData['tooltip-open-close-chart']);
    $(".chart-drag-handle i").prop('title', languageData['tooltip-click-drag-chart']);

    $(".y-axis-label").text(languageData['charts-y-axis-detections']);

    $("#records_loaded").html(languageData['loading-data-records']);

    $("div.ii i").tooltipster('content', languageData['tooltipster-content']);

    if($(".link-permalink").length > 0) {
        $(".link-permalink").trigger("permalink:change");
    }
}

function updateChartsVisibility() {
    var dimension_element;

    for (var dimension in WGL._dimensions) {
        dimension_element = WGL._dimensions[dimension];

        if (_instanceof(dimension_element, WGL.dimension.HistDimension) || _instanceof(dimension_element, WGL.dimension.FlagsDimension)) {
            var element = $("[data-name='" + dimension + "'] .chart-content");

            if (typeof element[0] !== "undefined") {
                if (dimension_element.visible && !visibleY(element[0])) {//dimension_element.setVisible(false);
                }

                if (!dimension_element.visible && visibleY(element[0])) {
                    //dimension_element.setVisible(true);
                    WGL.render();
                }
            }
        }
    }
}
