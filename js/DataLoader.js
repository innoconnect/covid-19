"use strict";

function DataLoader() {
    var instance; // singleton

    DataLoader = function DataLoader() {
        return instance;
    };

    DataLoader.prototype = this;
    instance = new DataLoader();
    instance.constructor = DataLoader;

    new WGL.ui.LoadingBar('overlay');
    $("#records_loaded").html("načítání dat");
    $(".overlay").show();
    $("#loading").show();

    instance.load = function (filename) {
        d3.json(filename, function (err, dataO) {
            let day = [];
            let nuts3 = [];
            let sex = [];
            let age = [];
            let country = [];

            const sexEnum = ["Z", "M"];

            let data = dataO.data;

            for (let j = 0; j < data.length; j++) {
                day[j] = data[j].DatumHlaseni;
                nuts3[j] = data[j].OkresKodBydliste; // .KHS
                sex[j] = data[j].Pohlavi;
                age[j] = data[j].Vek;

                let state = data[j].ImportZemeCsuKod;
                if (state == ""){
                    state = "CZ"
                }
                //country[j] = state;
                const rest = ["DE", "UA", "SK", "AT", "EG", "TZ", "RU", "AE", "PL", "ES", "IT", "CH", "RS", "BG", "GB", "FR", "CZ"];
                if (rest.includes(state)){
                    country[j] = state;
                }
                else{
                    country[j] = "other";
                }
            }

            const countryEnum = ["DE", "UA", "SK", "AT", "EG", "TZ", "RU", "AE", "PL", "ES", "IT", "CH", "RS", "BG", "GB", "FR", "other", "CZ"];
            //const countryEnum = country.filter((v, i, a) => a.indexOf(v) === i);

            const nuts3Enum = nuts3.filter((v, i, a) => a.indexOf(v) === i);
            const dayEnum = day.filter((v, i, a) => a.indexOf(v) === i).sort(function (a, b) {
                if (a > b) {
                    return 1;
                }
                if (b > a) {
                    return -1;
                }
                return 0;
            });

            vis({
                day: day,
                dayEnum: dayEnum,
                nuts3: nuts3,
                nuts3Enum: nuts3Enum,
                sex: sex,
                sexEnum: sexEnum,
                age: age,
                country: country,
                countryEnum: countryEnum,
            });
        }).on("progress", function() {
            if (d3.event.lengthComputable) {
                let percentComplete = Math.round(d3.event.loaded * 100 / d3.event.total);
                $(".progress-inner").css("width", percentComplete + "%");
            }
        });
        ;
    };

    instance.loadLanguageData = function(file) {
        d3.json(file, function(data) {
            renderLanguage(data);
        });
    };

    return instance;
}