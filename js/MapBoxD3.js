"use strict";

var mapMode = 1; // traffic level mode

var MapBoxD3 = function MapBoxD3(geojson_filename, data) {
    // MapBox map
    this.map = map;

    // source GeoJSON
    this.geojson = geojson_filename;
    this.isSelectSomething = false;
    this.selectedRegions = [];

    this.data = data;

    //Projection function
    var transform = d3.geo.transform({
        point: projectPoint
    });
    this.path = d3.geo.path().projection(transform);
    var path = this.path;

    function projectPoint(lon, lat) {
        var point = map.project(new mapboxgl.LngLat(lon, lat));
        this.stream.point(point.x, point.y);
    }

    this.mouseover = false;

    this.afterLoad = function () {};


    /**
     * get enumeration for 'c_prof'
     * @return {Array}
     */
    this.getEnum = function () {
        return this.bb.map(function (x) {
            return x.c_prof;
        });
    };

    d3.select("body").select("div.tooltip-map").remove();

    /**
     * draw lines to the map window and connect lines to the WGL
     */
    let div = d3.select("body").append("div").attr("class", "tooltip tooltip-map").style("opacity", 0)
        .html("<div id='region-name' style='font-weight: bold'></div><div id='region-first-line'><span class='region-relative-cases-text'>Cases per 100k: &nbsp;&nbsp;</span><span class='region-relative-cases'></span></div>"
        + "<div id='region-second-line'><span class='region-cases-text'>Total cases: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class='region-cases'></span></div>");

    this.draw = function () {
        var _this = this;

        queue().defer(d3.json, this.geojson).await(function (err, data) {
            var canvas = map.getCanvasContainer();
            var svg = d3.select(canvas).append("svg").attr("id", "mapsvg");
            _this.lines = svg.selectAll("path")
                .data(data.features).enter()
                .append("path")
                .attr("d", _this.path)
                .classed("polygon-base", true)
                .attr("id", function (d) {
                    return "kraj_" + d.properties.KOD_CZNUTS;
            }).on("mousemove", function (d) {
                _this.mouseover = true;
                div.transition().duration(100).style("opacity", .9);
                div.style("left", d3.event.pageX + "px").style("top", d3.event.pageY - 28 + "px");
                let cases = WGL.getDimension("kraje").readPixels();

                let pocet;
                let relative_pocet;
                for (let i = 0; i < cases.length; i++) {
                    if (cases[i].val === d.properties.KOD_CZNUTS) {
                        if (cases.sum_selected === 0){
                            pocet = cases[i].unselected;
                            relative_pocet =  pocet/(d.properties.POCET_OBYV+0.0)*100000;
                        }
                        else{
                            pocet = cases[i].selected;
                            relative_pocet = pocet/(d.properties.POCET_OBYV+0.0)*100000;
                        }
                    }
                }

                $(".region-relative-cases").text(Math.round(relative_pocet*10)/10);
                $(".region-cases").text(pocet);
                $("#region-name").text(d.properties.NAZEV);

            }).on("mouseout", function () {
                _this.mouseover = false;
                div.transition().duration(500).style("opacity", 0);

            }).on('click', function (d) {
                click_handler(d, d3.event.shiftKey);
            });

            // compute number of residents
            const dom = _this.data.nuts3Enum;
            let numOfResidents = [];

            for (let i = 0; i < dom.length; i++) {
                data.features.forEach(function(d){
                    if (dom[i] === d.properties.KOD_CZNUTS){
                        numOfResidents[i] = d.properties.POCET_OBYV;
                    }
                });
            }

            WGL.registerUpdateFunction(function () {
                function getColorLevel(value){
                    //var colors = [[0, 191, 87], [255, 222, 0], [243, 55, 25]];
                    //var colors = [[250, 247, 251], [125, 123, 194], [0, 0, 138]]; // blue
                    //var colors = [[250, 247, 251], [194, 123, 125], [138, 0, 0, 1]]; // red
                    //let colors = [[255, 255, 255], [125, 211, 245], [0, 176, 240, 1]]; // light blue
                    let colors = [[255, 255, 255], [231, 127, 155], [211, 18, 70]]; // KHS
                    let col1 = [];
                    let col2 = [];
                    let rangeval = 0.0;
                    if (value >= 0.5){
                        col1 = colors[2];
                        col2 = colors[1];
                        rangeval = (value - 0.5)*2.;

                    } else {
                        col1 = colors[1];
                        col2 = colors[0];
                        rangeval = value *2.0;

                    }

                    let col = [-1, -1, -1];
                    for(let i = 0;i<3;i++){
                        col[i] = col1[i]*rangeval + col2[i]*(1.0 - rangeval)
                    }
                    return "rgb("+col[0]+", "+col[1]+", "+col[2]+")"
                }

                let cases = WGL.getDimension("kraje").readPixels();

                _this.lines.attr("fill", function (d) {
                    let pocet = 0;
                    let max = 0.0;
                    for (let i = 0; i < cases.length; i++) {
                        let pocet_all;
                        if (cases.sum_selected === 0){
                            pocet_all =  cases[i].unselected/(numOfResidents[i]+0.0);
                        }
                        else{
                            pocet_all = cases[i].selected/(numOfResidents[i]+0.0);
                        }

                        if (cases[i].val === d.properties.KOD_CZNUTS) {
                            pocet = pocet_all;
                        }
                        if (pocet_all > max){
                            max = pocet_all;
                        }
                    }

                    $("#legend-right-number").html(Math.round(max*1000000)/10);

                    if (pocet > 0.0) {
                        //let value = Math.sqrt(pocet/max);
                        let value = pocet/max;
                        return getColorLevel(value);
                    } else {
                        return "rgb(255, 255, 255)";
                    }

                })

            });

            _this.afterLoad();

            $("#restart-button").click(e => {
                //console.log("click");
                _this.selectedRegions = [];
                WGL.resetFilters();
                WGL.resetFilters();
            });

            WGL.filterDim("kraje", "krajeF",[]);

            var shiftClick = jQuery.Event("click");
            shiftClick.shiftKey = true;
            var selected_segments = getUrlParameter("segments");

            if (selected_segments !== "") {
                selected_segments = selected_segments.split(",");

                for (var i = 0; i < selected_segments.length; i++) {
                    click_handler(parseInt(selected_segments[i]), true);
                }
            }
        });

        var update_map = function update_map() {
            if (typeof _this.lines !== "undefined") {
                _this.lines.attr("d", _this.path);
            }
        };

        map.off("viewreset").on("viewreset", update_map);
        map.off("move").on("move", update_map);

        map.off("moveend").on("moveend", function () {
            update_map();
        });

        map.off("click").on("click", function () {

            if (!_this.mouseover) {
                //console.log("click out");
                clean_map();
            }
        });

        let clean_map = function clean_map() {
            WGL.filterDim("kraje", "krajeF", []);
            _this.isSelectSomething = false;
            _this.selectedRegions = [];
        };

        let click_handler = function click_handler(d, shiftKey) {
            let kraj;

            if (d.properties !== undefined) {
                kraj = d.properties.KOD_CZNUTS;
            } else {
                kraj = d;
            }

            if (_this.selectedRegions.includes(kraj)){
                _this.selectedRegions = _this.selectedRegions.filter(item => item !== kraj);
                if(_this.selectedRegions.length === 0){
                    WGL.filterDim("kraje", "krajeF", []);
                }
                _this.selectedRegions.forEach((k,i) => {
                    if(i === 0){
                        WGL.exactFilterDim("kraje", "krajeF", k);
                    }
                    else{
                        WGL.exactFilterDim("kraje", "krajeF", k, true);
                    }
                });
            }
            else{
                if (_this.selectedRegions.length > 0){
                    WGL.exactFilterDim("kraje", "krajeF", kraj, true);
                }
                else{
                    WGL.exactFilterDim("kraje", "krajeF", kraj);
                }
                _this.selectedRegions.push(kraj);
            }
            // if (d3.select("#kraj_" + kraj).classed("polygon-selected")){
            //     WGL.exactFilterDim("kraje", "krajeF", kraj);
            //     d3.select("#kraj_" + kraj).classed("polygon-selected", false);
            // }
            // else{
            //     if (_this.isSelectSomething) {
            //         WGL.exactFilterDim("kraje", "krajeF", kraj, true);
            //     } else {
            //         WGL.exactFilterDim("kraje", "krajeF", kraj);
            //     }
            //     d3.select("#kraj_" + kraj).classed("polygon-selected", true);
            //     _this.isSelectSomething = true;
            // }
            // //d3.select("#kraj_" + kraj).classed("polygon-selected", true);
            // //_this.isSelectSomething = true;
            // WGL.exactFilterDim("kraje", "krajeF", kraj);

        };


    };
};