"use strict";

var map;
var draw;
var mapColor = 'light-v9';

function loadMap(dates) {

    const mobile = mobileCheck();

    var zoom = getUrlParameter("zoom");
    var center = getUrlParameter("center");
    mapboxgl.accessToken = 'pk.eyJ1IjoiamlyaS1iIiwiYSI6ImNqZmNjajc1MTJjN2cyeG5ycG5lcWhpNHMifQ.d-4wK9BUDPUHq_SRgHYe9g';
    var bounds = [
        [8.725781, 47.036767], // Southwest coordinates
        [23.863281, 52.454007] // Northeast coordinates
    ];
    map = new mapboxgl.Map({
        container: 'map',
        // container id
        style: 'mapbox://styles/mapbox/' + mapColor,
        // stylesheet location 'mapbox://styles/mapbox/dark-v9'
        center: center === "" ? [17.142877991919477, 49.798753375064166] : center.split(","),
        // starting position [lng, lat]
        zoom: zoom === "" ? 6.993493213391374 : zoom,
        // starting zoom,
        preserveDrawingBuffer: true,
        ratchety: false,
        maxZoom: 8,
        minZoom: (mobile ? 0 : 6.5),
        maxBounds: bounds
    });
    map.dragRotate.disable();
    map.touchZoomRotate.disableRotation();
    var dl = new DataLoader();
    dl.load("https://mapa-koronavirus.innoconnect.net/datacases/osoby.min.json");
}

$(document).ready(function () {
    loadMap();
});
