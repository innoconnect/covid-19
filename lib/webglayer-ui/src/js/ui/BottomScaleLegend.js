"use strict";

WGL.ui.BottomScaleLegend = function(parent_div_id, legend_text, legend_img) {

    this.parent_div = $("#"+parent_div_id);
    
    let bottomScaleLegend = 
        
        '<div id="bottomLegendWrapper" style="width: 100%; position: fixed; bottom: 0; z-index: 9999; border-radius: 5px; font-size: 13px; font-family: \'Ubuntu\', sans-serif; display: none">' +
            '<div id="bottomLegend" style="background-color: #122330; color: white; text-align: center; width: 21%; margin: 0 auto;">' +
                '<div id="bottomLegendText" style="padding: 8px;">'+legend_text+'</div>' +
                '<img alt="colours legend" src="'+legend_img+'" style="width: 100%">' +
            '</div>' +
        '</div>';

    this.parent_div.append(bottomScaleLegend);
};