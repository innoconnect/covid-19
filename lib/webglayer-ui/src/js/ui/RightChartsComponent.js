"use strict";

WGL.ui.RightChartsComponent = function(parent_div_id, div_id, charts_id) {

    let parent_div = $("#"+parent_div_id);

    let button = '<div id="charts-menu" class="charts-menu cursor-pointer" title="Expand charts panel"><em class="material-icons">bar_chart</em></div>';

    parent_div.append(button);

    let component =

        '<div id='+div_id+'  style="display: none" class="scrollbar style-1 scroll-bar-wrap">' +
            '<div>' +
                '<div id="charts-title" class="side-navbar-section " style="line-height: 1.8em; background-color: #122330">' +
                    '<div class="cursor-pointer height-title">' +
                        '<span class="charts-menu-active cursor-pointer float-left"><em class="material-icons line-height-title icon-margin-top-adjust" style="margin-top: 1px;">bar_chart</em></span>' +
                        '<span id="charts-panel-title" class="padding-left-half-em side-navbar-title line-height-title" >CHARTS</span>' +
                        '<span class="hamburger-menu-active cursor-pointer float-right"><em class="material-icons line-height-title icon-margin-top-adjust" style="margin-top: 1px; font-size: 18pt">arrow_forward_ios</em></span>' +
                    '</div>' +
                '</div>' +
                '<div id="'+charts_id+'" />' +
            '</div>' +
        '</div>';

    parent_div.append(component);

    $("#charts-menu, #charts-title").off("click").on("click", () => {
        $("#"+div_id).animate({width: 'toggle'});
        updateChartsVisibility();
    });

    this.setUpdateChartsVisibilityFunction = (f) => {
        updateChartsVisibility = f;
    };
    
    let updateChartsVisibility = () => {
            let dimension_element;
            for(let dimension in WGL._dimensions) {
                dimension_element = WGL._dimensions[dimension];
                if (dimension_element instanceof WGL.dimension.HistDimension
                    || dimension_element instanceof WGL.dimension.FlagsDimension) {
                    let element = $("[data-name='" + dimension + "'] .chart-content");

                    if (typeof element[0] !== "undefined") {

                        if (dimension_element.visible && !visibleY(element[0])) {
                            dimension_element.setVisible(false);
                        }

                        if (!dimension_element.visible && visibleY(element[0])) {
                            dimension_element.setVisible(true);
                            WGL.render();
                        }
                    }
                }
            }
    };

};