"use strict";

WGL.ui.WindowDialog = function(parent_div_id, window_name, visible) {

    this.parent_div = $("#"+parent_div_id);

    this.window_wrapper = $(".win-wrapper", "#"+parent_div_id);

    if(this.window_wrapper.length === 0) {
        let window_wrapper = '<div class="win-wrapper" ></div>';
        this.parent_div.append(window_wrapper);
        this.window_wrapper = $(".win-wrapper", "#"+parent_div_id);
    }

    let windowDialog =

        '<div id="'+window_name+'-win" class="wgl-'+window_name+'-win wgl-dialog-win" style="top: 0; left: 37%;">' +
            '<div class="wgl-'+window_name+'-head wgl-dialog-head"><span class="wgl-'+window_name+'-title">'+window_name.toUpperCase()+'</span>' +
                '<div class="wgl-'+window_name+'-close wgl-dialog-close"><em class="fa fa-times" aria-hidden="true"></em></div>' +
            '</div>' +
            '<div class="wgl-'+window_name+'-context wgl-dialog-context"></div>' +
        '</div>';

    this.window_wrapper.append(windowDialog);

    if(!visible) {
        $("#"+window_name+"-win").hide();
    }
};