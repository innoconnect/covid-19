"use strict";

WGL.ui.Map = function(parent_div_id, div_id) {

    this.parent_div = $("#"+parent_div_id);

    let map = '<div id="'+div_id+'" class="map fullscreen"></div>';

    this.parent_div.append(map);
};