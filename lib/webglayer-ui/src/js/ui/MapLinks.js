"use strict";

WGL.ui.MapLinks = function(parent_div_id) {

    this.parent_div = $("#"+parent_div_id);

    let mapLinks =

        '<div class="map-links" data-html2canvas-ignore>' +
            '<a href="http://innoconnect.net/" target="_blank">© InnoConnect 2018 | </a><a href="http://webglayer.org/" target="_blank">WebGLayer | </a><a href="https://www.mapbox.com/" target="_blank">© MapBox | </a><a href="https://www.openstreetmap.org/" target="_blank">© OpenStreetMap</a>' +
        '</div>';

    this.parent_div.append(mapLinks);

};