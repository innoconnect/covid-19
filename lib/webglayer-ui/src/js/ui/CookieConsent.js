"use strict";

WGL.ui.CookieConsent = function(parent_div_id) {

    this.parent_div = $("#"+parent_div_id);
    
    let cookieConsent = 
        
        '<div id="cookieConsentWrapper">' +
            '<div id="cookieConsent">' +
                '<div id="cookieMessage"></div><div><div class="cookieConsentOK"><a id="cookieOk"></a></div> <div class="cookieConsentDecline"><a id="cookieRefuse"></a></div></div>' +
            '</div>' +
        '</div>';

    this.parent_div.append(cookieConsent);

    let cookieConsentWrapper = $("#cookieConsentWrapper");

    if(getCookie("acceptCookies") === "") {
        setTimeout(() => cookieConsentWrapper.fadeIn(200), 4000);

        $(".cookieConsentOK").on("click", () => {
            setCookie("acceptCookies", "true");
            cookieConsentWrapper.fadeOut(200);
        });

        $(".cookieConsentDecline").on("click", () => cookieConsentWrapper.fadeOut(200));
    }
};