"use strict";

WGL.ui.HeatMapBoxes = function (m, div_id, filterId, params) {
    var _this = this;

    var that = this;
    var h;
    var permalink_input;
    var CELL_SIZE = 14;
    var NUMBER_OF_COLORS = 6;
    this.activeFilters = [];

    if (typeof params === "undefined") {
        h = 150;
        permalink_input = null;
    } else {
        h = params.h ? params.h : 180;
        permalink_input = params.permalink_input ? params.permalink_input : null;
    }

    var selectedColors = ["#059445", "#609E43", "#D6AB41", "#E8833A", "#D14C33", "#C0222D"];
    var unselectedColors = [[49 / 256, 130 / 256, 189 / 256], [158 / 256, 202 / 256, 225 / 256], [222 / 256, 235 / 256, 247 / 256]];
    var formatColor = d3.scaleQuantize().domain([m.minCount, m.maxCount]).range(d3.range(NUMBER_OF_COLORS).map(function (d) {
        return d;
    }));

    var getUnselectedColor = function getUnselectedColor(val) {
        var col1;
        var col2;
        var col;
        var rangeval;

        if (val >= 0.5) {
            col1 = unselectedColors[0];
            col2 = unselectedColors[1];
            rangeval = (val - 0.5) * 2.;
        } else {
            col1 = unselectedColors[1];
            col2 = unselectedColors[2];
            rangeval = val * 2.;
        }

        col1 = col1.map(function (a) {
            return a * rangeval;
        });
        col2 = col2.map(function (a) {
            return a * (1 - rangeval);
        });
        col = col1.map(function (a, index) {
            return a + col2[index];
        });
        return "rgb(" + col[0] * 256 + "," + col[1] * 256 + "," + col[2] * 256 + ")";
    };

    this.getDivId = function () {
        return div_id;
    };

    this.init = function () {
        var div_hm = $("<div id='heatmap-boxes-parent' class='js-heatmap' style='width: 100%;'></div>");
        $("#" + div_id).append(div_hm);
        var dateStart = m.startDate;
        var dateEnd = m.endDate;
        var dateStartRange = m.startDateRange;
        var dateEndRange = m.endDateRange;
        var month_selection = $("<div class='month-selection' id='month-selection' >" + "<span style='position: relative'>" + "<span><select id='select-date' class='select-calendar'>" + "<option value=\"last-30-days\">Last 30 days</option>" + "</select></span>" + "</span>" + "</div>");
        $("#heatmap-boxes-parent").append(month_selection);
        var dateText;
        var dateValue;
        var dateStartMoment = dateStart;
        var dateEndMoment = dateEnd;

        while (dateEndMoment > dateStartMoment || dateStartMoment.toFormat('M') === dateEndMoment.toFormat('M')) {
            dateText = dateEndMoment.toFormat('MMMM yyyy');
            dateValue = dateEndMoment.toFormat('MM-yyyy');
            var o = new Option(dateText, dateValue); /// jquerify the DOM object 'o' so we can use the html method

            $(o).html(dateText);
            $("#select-date").append(o);
            dateEndMoment = dateEndMoment.minus({
                months: 1
            });
        }

        var calendar_display = $("<div id='calendar-display' style='width: 100%;'></div>");
        $("#heatmap-boxes-parent").append(calendar_display);
        $("#heatmap-boxes-parent").append($("<div id='calendar-note' style='width: 100%'></div>"));
        $("#select-date").off("change").on("change", function () {
            var val = $(this).val();
            var lastMonth, lastYear;
            var startDate, endDate;

            if (val === 'last-30-days') {
                startDate = luxon.DateTime.local().toZone("Europe/Prague").minus({
                    days: 30
                }).toFormat("yyyy-MM-dd");
                endDate = luxon.DateTime.local().toZone("Europe/Prague").toFormat("yyyy-MM-dd");
            } else {
                lastMonth = parseInt(val.split("-")[0]);
                lastYear = parseInt(val.split("-")[1]);
                startDate = luxon.DateTime.local().setZone("Europe/Prague").set({
                    year: lastYear,
                    month: lastMonth,
                    hour: 0,
                    minute: 0,
                    second: 0,
                    millisecond: 0
                }).startOf('month').toFormat("yyyy-MM-dd");
                endDate = luxon.DateTime.local().setZone("Europe/Prague").set({
                    year: lastYear,
                    month: lastMonth,
                    hour: 0,
                    minute: 0,
                    second: 0,
                    millisecond: 0
                }).endOf('month').toFormat("yyyy-MM-dd");
            }

            delete $.data(document.body)['plugin_CalendarHeatmap'];
            reloadDateRange(new DataLoader(), startDate, endDate);
        });

        if (dateStartRange.toString() == luxon.DateTime.local().setZone("Europe/Prague").minus({
            days: 30
        }).startOf('day').toString() && dateEndRange.toString() == luxon.DateTime.local().setZone("Europe/Prague").startOf('day').toString()) {
            $("#select-date").val('last-30-days');
            $("#calendar-display").CalendarHeatmap(m.data, {
                months: 1,
                displayLast30Days: true,
                labels: {
                    days: true,
                    months: false
                },
                legend: {
                    show: true
                }
            });
            var days_calendar = $(".ch-day:not(.is-outside-month)");

            for (var i = 0; i < days_calendar.length; i++) {
                $(days_calendar[i]).append($("<text>" + moment().subtract(30 - i, 'days').date() + "</text>"));
                $(days_calendar[i]).addClass("ch-day-selected");
            }
        } else {
            var month = dateStartRange.month;
            var year = dateStartRange.year;
            $("#select-date").val(("0" + month).slice(-2) + "-" + year);
            $("#calendar-display").CalendarHeatmap(m.data, {
                months: 1,
                displayLast30Days: false,
                lastMonth: month,
                lastYear: year,
                labels: {
                    days: true,
                    months: false
                },
                legend: {
                    show: true
                }
            });
            var days_calendar = $(".ch-day:not(.is-outside-month)");

            for (var i = 0; i < days_calendar.length; i++) {
                $(days_calendar[i]).append($("<text>" + (i + 1) + "</text>"));
                $(days_calendar[i]).addClass("ch-day-selected");
            }
        }

        $(".is-outside-month:not(.is-outside-month-start)").remove();
        $(".ch-day:not(.is-outside-month-start):not(.ch-day-no-data)").off("click").on("click", function (e) {
            var target = e.target;

            if (!target.classList.contains("ch-day")) {
                target = target.parentElement;
            }

            var index = parseInt($(target).attr("data-index"));

            var filterIndex = _this.activeFilters.findIndex(function (a) {
                return a[0] === index;
            });

            if (filterIndex === -1) {
                _this.activeFilters.push([index, index + 1]);

                WGL.filterDim('date', 'dateF', _this.activeFilters);
            } else {
                _this.activeFilters.splice(filterIndex, 1);

                WGL.filterDim('date', 'dateF', _this.activeFilters);
            }

            updateFiltersHeader(_this.activeFilters.length);
            /*if($(this).hasClass("ch-day-selected")) {
            } else {
               let index = $(this).attr("data-index");
               activeFilters.push([index, index + 1]);
               WGL.filterDim('date', 'dateF', activeFilters);
            }*/
        });
        var filter_clean = $("#chd-container-" + div_id + " .chart-filters-clean");
        filter_clean.off("click").on("click", function (e) {
            e.stopPropagation();
            that.clearSelection();
        });
        $(".dropdown_arrow").off("click").on("click", function () {
            return $(".select-calendar").click();
        });
    };

    this.update = function () {
        if ($("#heatmap-boxes-parent").length === 0) {
            _this.init();
        }

        if (_this.activeFilters.length > 0) {
            $(".ch-day").removeClass("ch-day-selected");

            for (var i = 0; i < _this.activeFilters.length; i++) {
                $("div.ch-day[data-index=" + _this.activeFilters[i][0] + "]").addClass("ch-day-selected");
            }

            return;
        }

        var selected = WGL.getDimension("date").readPixels().filter(function (a) {
            return a.selected > 0;
        });

        if (selected.length === 0 && WGL.getNumberOfActiveFilters() === 0) {
            $(".ch-day").addClass("ch-day-selected");
            return;
        }

        if (WGL.getDimension("hours").filters["hoursF"].actual_filtres.length > 0 && (WGL.getDimension("days").filters["daysF"].actual_filtres.length === 0 || WGL.getDimension("days").filters["daysF"].actual_filtres.length === 7)) {
            $(".ch-day").addClass("ch-day-selected");
            return;
        }

        $(".ch-day").removeClass("ch-day-selected");

        for (var _i = 0; _i < selected.length; _i++) {
            $("div.ch-day[data-date=" + selected[_i]['val'] + "]").addClass("ch-day-selected");
        }
    };

    this.clean = function () {
        this.clearSelection();
    };

    this.clearSelection = function () {
        $(".ch-day:not(.is-outside-month-start)").addClass("ch-day-selected");
        this.activeFilters.length = 0;
        WGL.filterDim("date", "dateF", this.activeFilters);
        updateFiltersHeader(0);
    };

    var updateFiltersHeader = function updateFiltersHeader(selected) {
        $("#chd-container-" + div_id + " .chart-filters-selected").html(selected);

        if (selected > 0) {
            $("#chd-container-footer-" + div_id + " .chart-filters-selected").html(selected);
            $("#chd-container-footer-" + div_id).removeClass("hide");
        } else {
            $("#chd-container-footer-" + div_id).addClass("hide");
        }

        if ($(".active-filters-container [id^=chd-container-footer]:not(.hide)").length > 0) {
            $(".close-item-filters").removeClass("hide");
            $("#active-filters-placeholder").addClass("hide");
            $(".active-filters-item .bar-item").addClass("bar-item-active");
            $(".active-filters-container").slideDown();
        } else {
            $("#active-filters-placeholder").removeClass("hide");
            $(".close-item-filters").removeClass("hide");
        }

        if ($(".link-permalink").length > 0) {
            $(".link-permalink").trigger("permalink:change");
        }
    };
};