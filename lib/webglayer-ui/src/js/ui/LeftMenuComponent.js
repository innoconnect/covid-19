"use strict";

WGL.ui.LeftMenuComponent = function(parent_div_id, div_id) {

    this.parent_div = $("#"+parent_div_id);

    let button = '<div id="hamburger-menu" class="hamburger-menu cursor-pointer" title="Expand settings panel"><em class="material-icons">menu</em></div>';

    this.parent_div.append(button);

    let component =
        
        '<div id="'+div_id+'" class="side-navbar-wrapper hidden">' +
            '<div class="side-navbar-content">' +
                '<div id="navbar-title" class="side-navbar-section  " style="line-height: 1.8em">' +
                    '<div class="cursor-pointer height-title">' +
                        '<span class="hamburger-menu-active cursor-pointer float-left"><em class="material-icons line-height-title icon-margin-top-adjust" style="margin-top: 1px;">menu</em></span>' +
                        '<span id="side-navbar-title" class="padding-left-half-em side-navbar-title line-height-title" >SETTINGS</span>' +
                        '<span class="hamburger-menu-active cursor-pointer float-right"><em class="material-icons line-height-title icon-margin-top-adjust" style="margin-top: 1px; font-size: 18pt">arrow_back_ios</em></span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>';

    this.parent_div.append(component);

    $(".hamburger-menu, #navbar-title").off("click").on("click", () => {
        $("#side-navbar-content").animate({width: 'toggle'});
    });

    this.content_div = $("#"+div_id);

    this.addAboutSection = (click_cb) => {

        let aboutSection =
            
            '<div class="side-navbar-section  ">' +
                '<div id="about-section" class="cursor-pointer height-default">' +
                    '<span class="float-left cursor-pointer"><em class="material-icons line-height-default icon-margin-top-adjust">subject</em></span>' +
                    '<span id="about-text" class="padding-left-half-em line-height-default">About</span>' +
                '</div>' +
            '</div>';

        this.content_div.append(aboutSection);

        $("#about-section").off("click").on("click", click_cb);
    };

    this.addLegendSection = (click_cb) => {

        let legendSection =

            '<div class="side-navbar-section  ">' +
                '<div id="legend-section" class="cursor-pointer height-default">' +
                    '<span class="float-left cursor-pointer"><em class="material-icons line-height-default icon-margin-top-adjust">closed_caption</em></span>' +
                    '<span id="legend-text" class="padding-left-half-em line-height-default">Legend</span>' +
                '</div>' +
            '</div>';

        this.content_div.append(legendSection);

        $("#legend-section").off("click").on("click", click_cb);
    };

    this.addMapColorSection = (click_cb, click_option_cb) => {

        let mapColorSection =
            
            '<div class="side-navbar-section">' +
                '<div class="cursor-pointer height-default" id="background-section">' +
                    '<span class="float-left cursor-pointer"><em class="material-icons line-height-default icon-margin-top-adjust">map</em></span>' +
                    '<span id="background-section-text" class="padding-left-half-em line-height-default">Background map</span>' +
                '</div>' +
                    '<div id="background-options" style="display: none; margin: 5px">' +
                        '<div id="background-dark" class="side-navbar-section-options cursor-pointer layer-selected background-option" data-scheme="dark">Dark</div>' +
                        '<div id="background-light" class="side-navbar-section-options cursor-pointer background-option" data-scheme="light">Light</div>' +
                    '</div>' +
            '</div>';

        this.content_div.append(mapColorSection);

        $("#background-section").off("click").on("click", click_cb);

        $(".background-option").off("click").on("click", e => click_option_cb($(e.target).attr("data-scheme")));
    };

    this.addSelectLanguageSection = (click_cb, languages, click_option_cb) => {

        let selectLanguageSection =

            '<div class="side-navbar-section">' +
                '<div class="cursor-pointer height-default" id="language-section">' +
                    '<span class="float-left cursor-pointer"><em class="material-icons line-height-default icon-margin-top-adjust">language</em></span>' +
                    '<span id="language-section-text" class="padding-left-half-em line-height-default">Language</span>' +
                '</div>' +
                '<div id="language-options" style="display: none; margin: 5px"></div>' +
            '</div>';

        this.content_div.append(selectLanguageSection);

        $("#language-section").off("click").on("click", click_cb);

        let languageOptions = $("#language-options");

        for(let i in languages) {
            let languageOption = '<div data-language="' + i + '" id="language-option-'+ i +'" class="side-navbar-section-options cursor-pointer language-option">' + languages[i] + '</div>';
            languageOptions.append(languageOption);
        }

        $(".language-option").off("click").on("click", e => click_option_cb($(e.target).attr("data-language")));
    };

    this.addShareMapSection = (click_cb) => {
        
        let shareMapSection =
            
            '<div class="side-navbar-section">' +
                '<div id="share-section" class="link-permalink cursor-pointer height-default">' +
                    '<span class="float-left cursor-pointer"><em class="share-icon material-icons line-height-default icon-margin-top-adjust">link</em></em></span>' +
                    '<span id="share-text" class="padding-left-half-em line-height-default">Share this map</span>' +
                '</div>' +
                    '<div id="share-options" style="margin: 5px" class="hide">' +
                        '<div id="field-permalink" class="hide"><input id="permalink-input" class="text-permalink display-table-cell-center" type="text" readonly/><em class="fa fa-spinner fa-spin spinner-permalink hide"></em><em class="fa fa-copy copy-permalink" title="Copy link to clipboard"></em></div>' +
                    '</div>' +
            '</div>';

        this.content_div.append(shareMapSection);

        $("#share-section").off("click").on("click", click_cb);

        $("#permalink-input").val(window.location.href);

        $(".link-permalink").off("permalink:change").on("permalink:change", () => {
            $("#share-options").slideUp();
            $("#field-permalink").hide().addClass("hide");
            $("#permalink-input").val("");
        });

        $(".copy-permalink").off("click").on("click", () => {
            let el = document.querySelector(".text-permalink");
            el.focus();
            el.select();
            document.execCommand('copy');
        });
    };

    this.addExportDataSection = (click_cb) => {

        let exportDataSection =

            '<div class="side-navbar-section  ">' +
                '<div id="export-section" class="cursor-pointer height-default">' +
                    '<span class="float-left cursor-pointer"><em class="export-csv-icon material-icons line-height-default icon-margin-top-adjust">cloud_download</em><em class="fa fa-spinner fa-spin spinner-export-csv hide" title="Wait... Generating CSV..."></em></span>' +
                    '<span id="export-text" class="padding-left-half-em line-height-default">Export selected data</span>' +
                '</div>' +
            '</div>'

        this.content_div.append(exportDataSection);

        $("#export-section").off("click").on("click", click_cb);
    };
    
    this.addFeedbackSection = (href) => {
        
      let feedbackSection =
      
            '<div class="side-navbar-section">' +
                '<div class="cursor-pointer height-default">' +
                    '<a href="'+href+'" class="feedback-link">' +
                        '<span class="float-left cursor-pointer "><em class="material-icons line-height-default icon-margin-top-adjust">email</em></span>' +
                        '<span id="feedback-section-text" class="padding-left-half-em line-height-default">Send feedback</span>' +
                    '</a>' +
                '</div>' +
          '</div>';

      this.content_div.append(feedbackSection);
    };

    this.addCustomSection = (click_cb, section_html, section_id) => {
        this.content_div.append(section_html);
        $("#"+section_id).off("click").on("click", click_cb);
    }


};