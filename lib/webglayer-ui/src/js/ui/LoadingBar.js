"use strict";

WGL.ui.LoadingBar = function(parent_div_id) {

    this.parent_div = $("#"+parent_div_id);
    
    let loadingBar =
        
        '<div id="loading" class="loading-wgl">' +
            '<div><span id="records_loaded" class="text_map"><strong>-</strong> data records loaded</span></div>' +
            '<div class="progress-outer">' +
                '<div class="progress-inner">' +
            '</div>' +
            '</div>' +
        '</div>';

    this.parent_div.append(loadingBar);
};