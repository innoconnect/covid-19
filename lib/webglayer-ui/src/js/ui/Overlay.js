"use strict";

WGL.ui.Overlay = function(parent_div_id) {

    this.parent_div = $("#"+parent_div_id);

    let overlay = '<div id="overlay" class="overlay"><div class="overlay-spinner" style="display: none"><i class="material-icons spin">autorenew</i></div></div>';

    this.parent_div.append(overlay);
};